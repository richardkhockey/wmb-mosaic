  <style>
    ul.mosiac{
      width: 100%;
      list-style-type: none;
      padding: 0;
      display: flex;
      flex-wrap: wrap;
      margin: 0;
      background-image: url('http://local.mbastack/wp-content/uploads/2021/07/culture-1.jpg');
      background-repeat: no-repeat;
      background-size:cover ;
      background-postion:50% 50%;
    }
    ul.mosiac li{
      width: calc(100% / 32);
      flex-basis: calc(100% / 32);
      height: calc(100vh / 20);
      overflow: hidden;
      background-image: url('http://local.mbastack/wp-content/uploads/2021/07/people-stephen-maher.jpg');
      background-repeat: no-repeat;
      background-size:cover ;
      background-position:50% 50%;
      opacity: 0.5;
      transition: opacity 0.3s ease;
      cursor: pointer;
    }

    ul.mosiac li .yellow{
      width: 100%;
      height: 100%;
      opacity: 0;  
      background-color: #fff600;
      transition: opacity 0.3s ease;
    }
    ul.mosiac li:hover{
      opacity: 0.8;
    }
    ul.mosiac li:hover .yellow{
      opacity: 0.5;  
    }

    ul.mosiac li figure{
      display: none;
    }
    /*
    ul.mosiac li figure{
      display: none;
      height: 100%;
      position: relative;
      margin: 0;
    }
    ul.mosiac li figure img{
      position:absolute;
      left:50%;
      top:50%;
      transform:translate(-50%,-50%);
      width: 100%;
      object-fit:cover;
    }
    ul.mosiac li:hover figure{
      opacity: 0.8;
    }
    */
  </style>
    
  <ul class="mosiac">
  <?php
  $n = 640; // 32 x 20
  while($n > 0) {
  ?>
  <li><div class="yellow"></div><figure><img alt="<?php echo $n; ?>" title="<?php echo $n; ?>" src="http://local.mbastack/wp-content/uploads/2021/07/people-stephen-maher.jpg"></figure></li>
  <?php
  $n--;
  }
  ?>
  </ul>
