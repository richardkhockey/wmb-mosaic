/* ------------------------------------------------ */
// Queue
/* ------------------------------------------------ */

function Queue(parameters) {
	this.parameters = parameters;
	this.items = parameters.items || [];
	this.settings = parameters.settings || {};
	this.actions = parameters.actions || {};
	this.workerLimit = parameters.settings.limit;


	this.itemCount = 0;
	this.activeWorkers = 0;
	this.workerIndex = 0;
	this.workers = [];

	this.init();
}

Queue.prototype = {
	"constructor" : Queue,
	"template" : function () {var that = this; },
	
	"init" : function () {
		var that = this;

		// console.log('Queue.init');
		// console.log(this.settings);
	},
	"addItem" : function (newItem) {
		var that = this;

		this.items.push(newItem);
		this.itemCount++;
	},
	"clearQueue" : function () {
		var that = this;

		this.workers = [];
		this.itemCount = 0;
	},
	"startQueue" : function () {
		var that = this,
			worker;

		this.actions.queueStart();

		while (this.items.length > 0 && this.activeWorkers < this.workerLimit ) {
			worker = this.items.pop();
			this.workerStart(worker);
		}
	},
	"stopQueue" : function () {
		var that = this;
	},
	"workerStart" : function (worker) {
		var that = this;

		this.activeWorkers++;
		worker.wi = this.workers.length;
		this.workers[this.workers.length] = worker;

		this.actions.itemStart(this, worker);

	},
	"workerComplete" : function (worker) {
		var that = this,
			worker;

		this.actions.itemComplete(this,worker);

		// clear finished worker
		this.activeWorkers--;
		this.workers.splice(worker.wi);

		// are there items left, and worker slots available, if so start unassigned item 
		while (this.items.length > 0 && this.activeWorkers < (this.workerLimit + 1) ) {
			worker = this.items.pop();
			this.workerStart(worker);
		}

		// all items done
		if (this.activeWorkers == 0 && this.items.length == 0) {
			this.queueComplete();
		}

	},
	"queueComplete" : function () {
		var that = this;

		this.actions.queueComplete(this);
	}
}

export default Queue;