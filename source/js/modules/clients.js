		/* ---------------------------------------------------------------------------- */
		// clients page
		// dependent: /clients.html

		if ( $('body').hasClass('clients') ) {
			// add touch events to show client info overlays

			// queue clients logos for staggered load
			/*

			let logo = $('article div.logo').attr(data-logo)
			$('article div.logo').css({ "background-image" : "url('dist/img/clients/" + logo + ".png');" });
			*/

			// queue: load all people images then fade in page elements
			let logoQueue = new QueueSingle({
				"settings" : {
					"delay" : 250, // quarter second delay between each person fades in
					// "logopath" : "/uploads/assets/images/clients/"
					"logopath" : ""
				},
				"actions" : {
					"queueStart" : function(queue){},
					"itemStart" : function(queue,worker){

						let logoFile = queue.settings.logopath + worker.logo;

						// load image into hidden image cache
						let logoImage = $('<img></img>').attr({
							"alt" : worker.title,
							"src" : logoFile
						})
						.on('load',function(e){
							worker.delay = window.setTimeout(function(){
								queue.workerComplete();	
							}, queue.settings.delay);
						})
					    .on('error', function(e) {
							// image did not load
							worker.client.addClass('brokenImage');
							worker.delay = window.setTimeout(function(){
								queue.workerComplete();	
							}, queue.settings.delay);
					    });

						$('.moduleCache').append(logoImage);
					},
					"itemComplete" : function(queue,worker){
						worker.client.addClass('ready');
						worker.logobox.css({ "background-image" : "url('" + queue.settings.logopath + worker.logo + "')" });
					},
					"queueComplete" : function(queue){
						// temporarily disable focus states for client logos until all copy is in place
						/*
						$('.clientlist article a.focus').each(function(i){
							$(this).on('click', function(e){
								e.preventDefault();
								let thisItem = $(this).parents('article').eq(0);
								// let thisInfo = thisItem.find('.info');
								if ( thisItem.hasClass('active') ) {
									thisItem.removeClass('active');
								} else {
									thisItem.addClass('active');
								}

							});
						});
						*/
					}
				}
			});

			$('section.clientlist article').each(function(i){
				let logoBox = $(this).find('div.logo').eq(0);
				let logoTitle = $(this).find('div.info h2').eq(0).html();
				logoQueue.addItem({
					"title" : logoTitle,
					"client" : $(this),
					"logobox" : logoBox,
					"logo" : logoBox.attr('data-logo')
				});
			});

			logoQueue.startQueue();

		}
