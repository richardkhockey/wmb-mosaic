/* default */

// localize_vars passed from function.php -> mbastack_enqueue_scripts

/* ----------------------------------- */

window.requestAnimFrame = (function(callback) {
	return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
	function(callback) {
		window.setTimeout(callback, 1000 / 60);
	};
})();

/* ----------------------------------- */

if (!window.console) {
	window.console = {
		"log" : function(){}
	}
}

/* ----------------------------------- */

(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

/* ----------------------------------- */

function Window(parameters) {
	this.parameters = parameters;

	this.redraw = parameters.redraw;

	this.init();
}

Window.prototype = {
	"constructor" : Window,

	"template" : function () { var that = this; },

	"init" : function () {
		var that = this;

		this.landscapeAspect = false,
		this.shortLandscape = false,
		this.windowSize = false;

		this.setDimensions();
		console.log(this.getDimensions());

		/*
		$(window).smartresize(function(){
			that.setDimensions();
		});

		window.onorientationchange = function() { 
			that.setDimensions();
		}
		*/

		$(window).smartresize(() => {
			this.setDimensions();
			this.redraw(this);
		});

		/*
		window.onorientationchange = () => {
			this.setDimensions();
			console.log(this.getDimensions());
		};
		*/
	},

	"setDimensions" : function () {
		var that = this;

		this.windowSize = {
			"w" : $(window).width(),
			"h" : $(window).height(),
			"a" : $(window).width() / $(window).height()
		};

		if (this.windowSize.a > 1) {
			$('html').removeClass('portraitAspectRatio');
			$('html').addClass('landscapeAspectRatio');
			this.landscapeAspect = true;

			if (this.landscapeAspect && this.windowSize.h < 500) {
				this.shortLandscape = true;
				$('html').addClass('shortLandscape');
			} else {
				this.shortLandscape = false;
				$('html').removeClass('shortLandscape');
			}
		} else {
			$('html').removeClass('landscapeAspectRatio');
			$('html').removeClass('shortLandscape');
			$('html').addClass('portraitAspectRatio');
			this.landscapeAspect = false;
		}

	},

	"getDimensions" : function () {
		var that = this;

		return {
			"w" : this.windowSize.w,
			"h" : this.windowSize.h,
			"aspect" : this.windowSize.a,
			"landscape" : this.landscapeAspect,
			"shortLandscape" : this.shortLandscape
		};

	}
}

/* ----------------------------------- */

function redrawGrid(mosaic,cellRequiredDimensions,photos,photoPath,windowTool) {
	console.log('redraw');
	let windowData = windowTool.getDimensions();
	console.log(windowData)

	// calculate columns
	let cellColumnCountRaw = windowData.w / cellRequiredDimensions.w;
	let cellColumnCount = Math.floor(cellColumnCountRaw); // round down
	let cellWidth = 100 / cellColumnCount; // get cell width in percent

	// calculate rows
	let cellRowCountRaw = windowData.h / cellRequiredDimensions.h;
	let cellRowCount = Math.floor(cellRowCountRaw); // round down
	let cellheight = 100 / cellRowCount; // get cell height in percent

	let cellcount = cellColumnCount * cellRowCount;
	console.log(cellColumnCount,cellRowCount);

	// build index of cells
	let cellIndex = [];
	let n = 0;
	while (n < cellcount) {
		cellIndex.push(n);
		n++;
	}

	// bind random photos to cells
	let photoMap = photos.map(function(item,index){return index;});
	let cellMap = cellIndex.map(function(item,index){return index;});

	let cellGrid = cellIndex.map(function(item,index){;
		return {
			"index" : index,
			"photo" : photos[ Math.floor(Math.random() * photos.length ) ],
			"active" : -1
		};
	});

	// select N random cells based on number of active items
	let activeItemCount = activeCells.length;
	let activeItemMap = activeCells.map(function(item,index){return index;});

	// select random cells
	let activeCellList = [];
	n = 0
	while( n < activeItemCount){
		let vi = Math.floor( Math.random() * cellMap.length );
		activeCellList.push( cellMap.splice( vi ,1 ) );
		n++;
	}

	// bind randonm active item in selected cells
	n = 0
	while (n < activeCellList.length) {
		cellGrid[activeCellList[n]].active = activeItemMap.splice(Math.floor( Math.random() * activeItemMap.length ),1);
		n++;
	}

	// render grid
	mosaic.addClass('hidden');
	mosaic.empty();
	n = 0;
	while ( n < cellcount ) {
		let thisCell = cellGrid[n];

		let newCell = $('<li></li>')
			.css({
				"width" : cellWidth + "%",
				"flex-basis" : cellWidth + "%",
				"height" : cellheight + "vh",
				"background-image" : "url('" + photoPath + thisCell.photo + "')"
			});

		if ( thisCell.active !== -1) {

			let activeItem = activeCells[thisCell.active];

			let yellowDiv = $('<div></div>')
				.addClass('yellow');

			let metaData = $('<div></div>')
				.addClass('metadata')
				.attr({
					"data-company" : activeItem.company,
					"data-photo" : activeItem.photo,
					"data-logo" : activeItem.logo,
					"data-name" : activeItem.name,
					"data-link" : activeItem.link
				})

			let metaDataCopy = $('<div></div>')
				.addClass('copy')
				.text(activeItem.copy);
			metaData.append(metaDataCopy);

			newCell
			.attr({
				"data-active" : thisCell.active
			})
			.css({
				"background-image" : "url('" + photoPath + activeItem.photo + "')"
			})
			.on('click',function(e){
				let cell = $(this);
				let meta = cell.find('.metadata')

				let company = meta.attr('data-company');
				let photo = meta.attr('data-photo');
				let logo = meta.attr('data-logo');
				let name = meta.attr('data-name');
				let link = meta.attr('data-link');
				let copy = meta.find('div.copy').html();

				// let popup = $('.popup');
				// #name .text()
				// #company .text()
				// #copy .htnl()
				// #link attr({'href'})
				// #photo attr({'src'})
				// #logo attr({'src'})
				$('#name').text(name);
				$('#company').text(company);
				$('#link').attr({'href' : link});
				$('#copy').text(copy);
				$('#logo').attr({'src' : 'assets/img/logos/' + logo, 'alt' : company});
				$('#photo').attr({'src' : 'assets/img/photos/' + photo, 'alt' : name});

				$('body').addClass('modal-open');
			});

			newCell
				.addClass('active')
				.append(yellowDiv)
				.append(metaData);
		}

		mosaic.append( newCell );
		n++;
	}
	mosaic.removeClass('hidden');
};

/* ----------------------------------- */

$(document).ready(function () {

	let mosaic = $('ul.mosiac');
	 // set min width and height of cells to 62px
	let cellRequiredDimensions = {
		"w" : 62,
		"h" : 62,
	}

	let photoPath = 'assets/img/photos/';
	//let photos = ["people-jack-gallon.jpg","people-james-devon.jpg","people-nicola-nimmo.jpg","people-richard-armstrong.jpg","people-rob-goodwin.jpg","people-stephen-maher.jpg"];
	//let logos = ["client-logos-acca.png","client-logos-dtree.png","client-logos-elanco.png","client-logos-interflora.png","client-logos-o2.png","client-logos-royal-mail.png","client-logos-sage.png","client-logos-solace.png"];
  

	let windowTool = new Window({
		"redraw" : function( WindowRef ){
			redrawGrid(mosaic,cellRequiredDimensions,photos,photoPath,WindowRef);
		}
	});

	// let mobileWidth = (windowData.w < 992) && (windowData.aspect < 1); // bootstrap desktop / mobile transition

	redrawGrid(mosaic,cellRequiredDimensions,photos,photoPath,windowTool);

/* ----------------------------------- */

	/*
	$('.mosiac li.active').on('click',function(e){
		let cell = $(this);
		let meta = cell.find('.metadata')

		let company = meta.attr('data-company');
		let photo = meta.attr('data-photo');
		let logo = meta.attr('data-logo');
		let name = meta.attr('data-name');
		let link = meta.attr('data-link');
		let copy = meta.find('div.copy').html();

		// let popup = $('.popup');
		// #name .text()
		// #company .text()
		// #copy .htnl()
		// #link attr({'href'})
		// #photo attr({'src'})
		// #logo attr({'src'})
		$('#name').text(name);
		$('#company').text(company);
		$('#link').attr({'href' : link});
		$('#copy').text(copy);
		$('#logo').attr({'src' : 'assets/img/logos/' + logo, 'alt' : company});
		$('#photo').attr({'src' : 'assets/img/photos/' + photo, 'alt' : name});

		$('body').addClass('modal-open');
	});
	*/

	// stop clcks on modal body passing through to modal backdrop
    $(".popup .modal-body").on("click", function(e) {
        // alert("video"),
        e.stopPropagation()
    });

	$('.popup button.close').on('click',function(e){
		$('body').removeClass('modal-open');
	});
	
/* ----------------------------------- */

	let isIOS = (/iPad|iPhone|iPod/.test(navigator.platform) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)) && !window.MSStream;
	// console.log(isIOS);
	if (isIOS) {
		$('html').addClass('is-ios');
	}

});
