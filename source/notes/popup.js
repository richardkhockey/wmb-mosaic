                $("#videopopup").addClass("portrait"),
                $(".popupVideo").on("click", (function(o) {
                    o.preventDefault();
                    let t = $(this)
                      , s = t.attr("data-title")
                      , i = t.attr("data-source")
                      , n = t.attr("data-poster");
                    // console.log("title:'%s' source:'%s' poster:'%s'", s, i, n),
                    newVideoTag = $("<video></video>"),
                    newVideoTag.attr({
                        preload: "metadata",
                        controls: "",
                        playsinline: "",
                        poster: n
                    }).addClass("waiting");
                    let a = $("<source></source>").attr({
                        type: "video/mp4",
                        src: i
                    });
                    newVideoTag.append(a),
                    $(".modal-body").empty().append(newVideoTag),
                    $("#videopopup").css({
                        display: "flex"
                    }),
                    $("#videoCaption").text(s),
                    $("body").addClass("modal-open")
                }
                ))

            $("#videopopup button.close").on("click", (function(o) {
                o.preventDefault(),
                $("body").removeClass("modal-open"),
                $("#videopopup").hide(),
                $(".modal-body").empty()
            }
            )),
            $(".modal-backdrop").on("click", (function(o) {
                o.preventDefault(),
                $("body").removeClass("modal-open"),
                $("#videopopup").hide(),
                $(".modal-body").empty()
            }
            )),
            $(".modal video").on("click", (function(e) {
                // alert("video"),
                e.stopPropagation()
            }
            )),
