<?php
  // $_SERVER['DOCUMENT_ROOT'] /Applications/MAMP/htdocs/
  // $_SERVER['DOCUMENT_URI'] 
  // wmb-mosiac/assets/img/photos/
  // wmb-mosiac/assets/img/logos/


  $server = $_SERVER['DOCUMENT_ROOT'];

  // index photos in assets/img/photos
  $photoPath = $server.'/wmb-mosaic/assets/img/photos/';
  $logoPath = $server.'/wmb-mosaic/assets/img/logos/';

  $photosRaw = scandir($photoPath);
  $logosRaw = scandir($logoPath);
  $photos = [];
  $logos = [];

  foreach ($photosRaw AS $item) {
    if ( preg_match('/^(.+)(\.jpg|\.png)$/i',$item, $matches ) ) {
      $photos[] = $matches[0];
    };
  }

  foreach ($logosRaw AS $item) {
    if ( preg_match('/^(.+)(\.jpg|\.png)$/i',$item, $matches ) ) {
      $logos[] = $matches[0];
    };
  }

  // define active hotspots which trigger the overlay
  $active = [
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco0',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco1',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco2',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco3',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco4',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco5',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco6',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco7',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco8',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco9',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco10',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco11',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco12',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco13',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco14',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ],
    [
      'name' => 'Aaron Aardvark',
      'photo' => 'people-jack-gallon.jpg',
      'company' => 'Exaco15',
      'logo' => 'client-logos-acca.png',
      'link' => 'http://www.google.com',
      'copy' => 'Lorem upsum'
    ]
  ];
  $activeKeys = range(0, ( count($active) - 1 ) );

  // define default size of grid (desktop 640 cells in 32 x 20 grid, 100% width by 100vh height)
  // need to define transition widths and number of cells to display as size of viewport decreases
  // also conside aspect ratio
  $n = 640; // 32 x 20

  // generate placeholder grid
  // select random images from $photos
  $grid = [];
  $cellKeys = range(0, ( $n - 1 ) );


  // select random photo to insert into grid from selection of placeholder photos
  while($n > 0) {
    $grid[] = [
      'photo' => $photos[array_rand($photos)],
      'active' => -1
    ];
    $n--;
  }

  // randomly place active hotspots in grid
  // select active cells
  $activeCells = [];
  foreach ( $active AS $index => $item) {
    $activeCells[] = array_splice($cellKeys, array_rand($cellKeys),1)[0];
  }

  // bind active item to selected cell
  $activeItems = []; // cell index, item
  $activeCompanies = $active;
  foreach ( $activeCells AS $index => $item) {
    $activeItems[] = [
        'cell' => $item,
        'company' => array_splice($activeKeys, array_rand($activeKeys),1)[0]
    ];
  }

  foreach ( $activeItems AS $item ) {
    $grid[$item['cell']]['active'] = $item['company'];
    // echo "<p>Bound cell ".$item['cell']." to company ".$item['company']."</p>\n";
  }

  /*
  <p>activeCells: <?php echo implode(', ',$activeCells); ?></p>
  <pre><?php echo print_r($activeItems,true); ?></pre>
  <pre><?php echo print_r($grid,true); ?></pre>
  */
  $grid = [];
?>
<!doctype html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name='robots' content='noindex, nofollow' />
    <link href="assets/css/styles.min.css" rel="stylesheet">
  </head>
  <body>
    <header class="navheader">
    </header>

    <section class="wmbGrid">

      <ul class="mosiac">
      <?php
      foreach ($grid AS $index => $cell ) :
      ?>
        <?php if ( $cell['active'] > -1 ) : ?>
          <li style="background-image:url('assets/img/photos/<?php echo $active[$cell['active']]['photo']; ?>');" class="active"><div class="yellow"></div>
            <!-- <figure><img alt="<?php echo $index; ?>" title="<?php echo $index; ?>" src="assets/img/photos/<?php echo $cell; ?>"></figure> -->
          <div class="metadata" data-company="<?php echo $active[$cell['active']]['company']; ?>" data-photo="<?php echo $active[$cell['active']]['photo']; ?>" data-logo="<?php echo $active[$cell['active']]['logo']; ?>" data-name="<?php echo $active[$cell['active']]['name']; ?>" data-link="<?php echo $active[$cell['active']]['link']; ?>">
            <div class="copy"><?php echo $active[$cell['active']]['copy']; ?></div>
          </div>
        </li>
        <?php else : ?>
        <li style="background-image:url('assets/img/photos/<?php echo $cell['photo']; ?>');"></li>
        <?php endif; ?>
      </li>
      <?php endforeach; ?>
      </ul>
    </section>

    <footer>
    </footer>

  <script type="text/javascript">
  var data = <?php echo json_encode($active); ?>;
  var photos = <?php echo json_encode($photos); ?>;
  var logos = <?php echo json_encode($logos); ?>;
  </script>
  <script src="assets/js/library/bootstrap.min.js" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script type='text/javascript' src='assets/json/active.js'></script>
  <script type='text/javascript' src='assets/js/default.min.js'></script>



    <div class="popup">

      <div class="modal-body">

        <div class="container">
          <button type="button" class="close">
            <span aria-hidden="true">×</span>
          </button>

          <div class="row">

            <div class="col-3">
              <figure class="company"><img alt="XXX" src="assets/img/logos/" id="logo"></figure>
            </div>

            <div class="col-9">
              <h2 id="company">XXX</h2>
              <p id="copy">XXX</p>
              <a href="" target="_blank" id="link">Read More &gt;</a>
            </div>

          </div>

          <div class="row">
            <div class="col-3">
              <figure class="person"><img alt="XXX" src="assets/img/photos/" id="photo"></figure>
            </div>
            <div class="col-9">
              <h2 id="name">XXX</h2>
            </div>

          </div>

        </div>
      </div>
    </div>
    <div class="modal-backdrop"></div>
  </body>
</html>
